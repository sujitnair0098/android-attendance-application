package attendance.helixtech.com.attendance;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.InputStream;
import java.util.prefs.Preferences;

public class MainActivity extends Activity {

    EditText editTextUserName ,editTextPassword ;
    ProgressBar progressBar;

    //String username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        editTextUserName = (EditText) findViewById(R.id.etext_username);
        editTextPassword = (EditText) findViewById(R.id.etext_password);



    }
    public void onLogin(View view){
        String username = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();


        SharedPreferences sharedPreferences=getSharedPreferences("MY PRESS",MODE_PRIVATE);

       // String Userdetails=Preferences.getText();



                   if (!android.util.Patterns.EMAIL_ADDRESS.matcher(editTextUserName.getText().toString()).matches()) {
                       //Validation for Invalid Email Address
                       Toast.makeText(getApplicationContext(), "Invalid Email", Toast.LENGTH_LONG).show();
                       editTextUserName.setError("Invalid Email");
                       return;
                   } else if (editTextPassword.getText().toString().length() == 0) {
                       Toast.makeText(getApplicationContext(), "Name cannot be Blank", Toast.LENGTH_LONG).show();
                       editTextPassword.setError("Name cannot be Blank");
                       return;
                   }
                   Intent intent = new Intent(MainActivity.this, SigninActivity.class);

                     // Closing all the Activities from stack
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                 // Add new Flag to start new Activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   startActivity(intent);
                //      login(username,password);
               }

 @Override
 public boolean onCreateOptionsMenu(Menu menu) {
     MenuInflater inflater = getMenuInflater();
     inflater.inflate(R.menu.menu, menu);
     return true;
 }

    public final boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

}
